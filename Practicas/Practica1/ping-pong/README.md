Ejercicio: Ping-Pong
====================

Si solo podemos enviar mensajes a procesos registrados no sería un
sistema transparente. Podemos enviar mensajes a cualquier proceso pero
el problema es obtener el identificador del proceso. No hay forma de
escribir el valor del identificador del proceso `<0.70.0>` que el shell
imprime.

El único proceso que conoce el identificador de proceso de un proceso es
el que lo crea y el propio proceso. El propio proceso puede obtener el
identificador llamando la BIF `self()`. Por supuesto podemos pasar
identificadores de procesos, ya sea como parámetros o enviarlos en un
mensaje a otro proceso conocido. Una vez que un proceso lo conoce puede
usar el identificador sin importar si es un proceso local o remoto.

Probar definir en una máquina un proceso ping, que envía un mensaje a un
proceso registrado en otra máquina con su propio identificador de
proceso. El proceso debe esperar por una respuesta. El receptor en la
otra máquina debe observar el mensaje y usar el identificador del
proceso para enviar una respuesta.

Una aclaración, la primitiva send `!` acepta tanto nombres registrados,
nombres remotos como identificadores de procesos. Esto puede causar
problemas algunas veces. Si implementamos un sistema concurrente que no
es distribuido y tenemos un proceso registrado con el nombre `foo`,
podemos pasar el atom `foo` y otros procesos tratarlo como un
identificador de proceso. Ahora si hacemos la aplicación distribuida un
proceso remoto no será capaz de utilizarlo como un identificador de
proceso dado que el proceso es local al nodo que lo registra. Por eso
debemos estar atentos de que lo que enviamos, es un identificador de
proceso (que puede ser usado en forma remota) o es un nombre bajo el
cual el proceso puede estar registrado.

### Prueba
Para probar el ejercicio se deben tener dos terminales abiertas con el indice en el directorio de la resolución alojada.
En la primer terminal escribir: 
```sh
erl -name bar@192.168.0.32 -setcookie secret
c(ping)
ping:startReceiver()
```
en el segundo terminar escribir:
```sh
erl -name foo@192.168.0.32 -setcookie secret
c(ping)
ping:startSender('bar@192.168.0.32')
```

con esto debería mostrar los resultados de ping y pong en cada una de las terminales.