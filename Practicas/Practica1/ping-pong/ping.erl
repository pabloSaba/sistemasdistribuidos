-module(ping).

%% API
-export([ping/1,pong/0,startReceiver/0,startSender/1]).

pong() ->
  io:fwrite("escuchando... \n"),
  receive
    {ping, IPSend} ->
      io:fwrite("solicitud de ping \n"),
      IPSend ! pong
  end.

ping(IPNode) ->
  Start = erlang:system_time(micro_seconds),
  {pong, IPNode} ! {ping, self()},
  receive
    pong ->
      Finish = erlang:system_time(micro_seconds),
      Finish - Start,
      io:fwrite("Pong recibido en "++ integer_to_list(Finish)  ++ " micro segundos\n")
end.

startReceiver() ->
  register(pong, spawn(ping, pong, [])).

startSender(IPNode) ->
  spawn(ping, ping, [IPNode]).