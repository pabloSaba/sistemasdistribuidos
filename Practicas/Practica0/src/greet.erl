%%%-------------------------------------------------------------------
%%% @author saba
%%% @copyright (C) 2020, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 19. abr. 2020 14:47
%%%-------------------------------------------------------------------
-module(greet).
-author("saba").

%% API
-export([greet/1]).

greet([]) ->
  true;

greet([First | Rest]) ->
  io:fwrite("Hello " ++ First ++ "\n"),
  greet(Rest).