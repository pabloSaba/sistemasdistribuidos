-module(temp).
-author("saba").

%% API
-export([convert/1]).

convert(F, fahrenheit) ->
  (F -32) * 5/9;
convert(C, celsius) ->
  (C +32 * 9/5).

convert({fahrenheit,Y}) ->
  X = convert(Y,fahrenheit),
  {celsius,X};
convert({celsius,Y}) ->
  X = convert(Y,celsius),
  {fahrenheit,X}.