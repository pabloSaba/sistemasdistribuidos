%%%-------------------------------------------------------------------
%%% @author saba
%%% @copyright (C) 2020, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 23. abr. 2020 01:06
%%%-------------------------------------------------------------------
-module(category).
-author("saba").

%% API
-export([getType/1]).

getType(N) when N < 13 ->
  child;
getType(N) when N < 18 ->
  teen;
getType(N) ->
  adult.