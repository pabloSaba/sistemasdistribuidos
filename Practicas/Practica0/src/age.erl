%%%-------------------------------------------------------------------
%%% @author saba
%%% @copyright (C) 2020, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 19. abr. 2020 13:32
%%%-------------------------------------------------------------------
-module(age).
-author("saba").

%% API
-export([getAge/1]).


getAge(Name) ->
  AgeName = #{"Alice" => 23,"Pablo" =>33, "Carol" => 35},
  maps:get(Name,AgeName, -1).