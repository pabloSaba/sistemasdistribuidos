%%%-------------------------------------------------------------------
%%% @author saba
%%% @copyright (C) 2020, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 23. abr. 2020 01:54
%%%-------------------------------------------------------------------
-module(speak).
-author("saba").

%% API
-export([run/0, say/2]).
say(What, 0) ->
  done;
say(What, Times) ->
  io:fwrite(What ++ "\n"),
  say(What, Times -1).

run() ->
  spawn(speak, say, ["Hi", 3]),
  spawn(speak, say, ["Bye", 3]).