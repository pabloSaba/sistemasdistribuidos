%%%-------------------------------------------------------------------
%%% @author saba
%%% @copyright (C) 2020, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 23. abr. 2020 13:21
%%%-------------------------------------------------------------------
-module(talk2).
-author("saba").

%% API
-export([run/0, escribirputo/0]).

escribirputo() ->
  receive
    puto -> io:fwrite("eeh gil");
    lindo -> io:fwrite("ahora viene lo bueno jovenes")
  end.


run() ->
  PId = spawn(talk2, escribirputo,[]),
  PId ! lindo,
  PId ! puto.

