-module(rudy).
-author("saba").

%% API
-export([start/1,stop/0]).

%%% ----------------------------init(Port)----------------------------- %%%
%%%                                                                     %%%
%%% el procedimiento que inicializará el servidor, toma un              %%%
%%% número de puerto (por ejemplo 8080), abre un socket en modo escucha %%%
%%% y pasa el socket a handler/1. Una vez que el request fue procesado  %%%
%%% el socket se cerrará.                                               %%%
init(Port) ->
  Opt = [list, {active, false}, {reuseaddr, true}],
  case gen_tcp:listen(Port, Opt) of
    {ok, Listen} ->
      handler(Listen),
      gen_tcp:close(Listen),
      ok;
    {error, Error} ->
      io:format("rudy: error: ~w~n", [Error]),
      error
  end.

%%% ----------------------- -handler(Listener)----------------------------- %%%
%%%                                                                         %%%
%%% handler(Listen): escuchará el socket esperando una conexión de entrada. %%%
%%% Una vez que un cliente se conecta pasara la conexión a request/1.       %%%
%%% Cuando el request haya sido procesado se cerrará la conexión.           %%%


handler(Listen) ->
  case gen_tcp:accept(Listen) of
    {ok, Client} ->
        spawn(fun() ->request(Client) end),
      handler(Listen),
      true;
    {error, Error} ->
      io:format("rudy: error: ~w~n", [Error]),
      error
  end.

%%% --------------------------------request(Client)-------------------------------- %%%
%%%                                                                                 %%%
%%% request(Client): leerá el request desde la conexión del cliente y la parseará.  %%%
%%% Usara nuestro http parser y pasará la request a reply/1. La respuesta luego     %%%
%%% es enviada al cliente.                                                          %%%

request(Client) ->
  Recv = gen_tcp:recv(Client, 0),
  case Recv of
    {ok, Str} ->
      ParseRecuest = http:parse_request(Str),
      Response = reply(ParseRecuest),
      gen_tcp:send(Client, Response);
    {error, Error} ->
      io:format("rudy: error: ~w~n", [Error])
  end,
  gen_tcp:close(Client).

%%% -------------------------------Reply(Request)-------------------------------- %%%
%%%                                                                               %%%
%%% reply(Request): es donde decidimos qué responder, como convertir la respuesta %%%
%%% en una respuesta HTTP bien formada.                                           %%%

reply({{get, URI, _}, _, _}) ->
  timer:sleep(40),
  http:ok(URI).


start(Port) ->
  register(rudy, spawn(fun() -> init(Port) end)).

stop() ->
  exit(whereis(rudy), "time to die").
