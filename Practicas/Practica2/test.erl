-module(test).
-export([bench/2]).

bench(Host, Port) ->
  Start = erlang:system_time(micro_seconds),
  PID = run(1000, Host, Port),
  Finish = erlang:system_time(micro_seconds),
  Finish - Start.

run(0, _, _) ->
  ok;

run(N, Host, Port) ->
  Pid =spawn(fun() -> request(Host, Port) end),
  io:format("test: : ~w~n", [N]),
  Pid ! run(N-1, Host, Port).


request(Host, Port) ->
  Opt = [list, {active, false}, {reuseaddr, true}],
  {ok, Server} = gen_tcp:connect(Host, Port, Opt),
  gen_tcp:send(Server, http:get("foo")),
  Receiver = gen_tcp:recv(Server, 0),
  case Receiver of
    {ok, _} ->
      ok;
    {error, Error} ->
      io:format("test: error: ~w~n", [Error])
  end,
  gen_tcp:close(Server).