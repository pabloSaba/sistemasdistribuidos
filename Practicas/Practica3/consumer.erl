-module(consumer).
-author("saba").

%% API
-export([start/1,stop/0,run/0]).

start(Producer) ->
  Monitor = erlang:monitor(process, Producer),
  Producer ! {hello, self()},
  consumer(0, Monitor).

consumer(N, Monitor)->
  receive
    {'DOWN', Monitor, process, Object, Info} ->
      io:format("~w died; ~w~n", [Object, Info]),
      consumer(N, Monitor);

    {ping, R} ->
      if
        N == R ->
          io:format("ping: : ~w~n", [N]);
        true ->
          io:format("warning: : ~w~n ~w~n", [N, R])
      end,
      consumer(N+1, Monitor);
    bye ->
      erlang:demonitor(Monitor, [flush, info]),
      ok
  after 10000 ->   io:fwrite("TimeOut")
  end.




stop() ->
  consumer ! stop.

run() ->
  spawn(fun() -> start({producer,'silver@Flow'}) end).
