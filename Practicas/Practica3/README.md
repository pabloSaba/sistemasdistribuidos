# Detector: detección de fallas en Erlang

###Instrucciones
Ubicarse en la raiz.

Recordar cambiar el host correspondiente en el consumer.
```erlang
  spawn(fun() -> start({producer,'silver@Flow'}) end).
```
compilar el producer y el consumer

se levanto el producer ...
```shell script
$ erl
$ c(producer).
$ c(consumer).
```
luego levantamos 2 nodos
```shell script
$ erl -sname gold -setcookie foo

```
```shell script
$ erl -sname silver -setcookie foo

```
en cada uno vamos a levantar nuestro consumer y nuestro producer(no en ese orden)
```shell script
(silver@Flow)3> producer:run().
```
```shell script
(gold@Flow)3> consumer:run().
```

Con eso deberia funcionar como correspondientemente.


En el mismo host
----------------


Luego de seguir las instrucciones, no solo podemos simular un crash en el producer, sino ademas matar
el nodo Erlang donde está corriendo. 



Desde el host donde se ejecuta el producer vamos a simular el crash
```shell script
(silver@Flow)4> producer:chash().
```
¿Qué mensaje se da como razón
cuando el nodo es terminado?
```erlang
{producer,silver@Flow} died; {badarith,[{producer,producer,3,[{file,[112,114,111,100,117,99,101,114,46,101,114,108]},{line,29}]}]}
```
 ¿Porqué?
 
 Esta respuesta es a razón de que el monitor detecta que el nodo al que estabamos conectados crasheo.
 
 Pero que pasa si el producer no llega a avisar que crasheo?

Un experimento distribuido
--------------------------

Ahora las cosas se están poniendo interesantes; ejecutemos cada uno de
los nodos en diferentes computadoras. En principio todo debería ser
normal y deberíamos ser capaces de provocar un crash en el proceso. ¿Qué
sucede si matamos el nodo Erlang en el producer?

 ```erlang
 {producer,silver@Flow} died; noconnection
 ```
En efecto, el monitor detecta de que perdió conexión y notifica al receiver del consumer que este proceso no va  responder
 

Ahora probemos desconectar el cable de red de la máquina corriendo el
producer y volvamos a enchufarlo despues de unos segundos. ¿Qué pasa?
Desconectemos el cable por períodos mas largos. ¿Qué pasa ahora?

Si lo desconectamos en un periodo corto y lo volvemos a conectar.

Pero en un periodo mas largo este muere.

¿Qué significa haber recibido un mensaje `’DOWN’`? ¿Cuándo debemos
confiar en él?

Considero que siempre... no veo porque desconfiar de `'DOWN'`

¿Se recibieron mensajes fuera de orden, aun sin haber recibido un
mensaje `’DOWN’`? ¿Qué dice el manual acerca de las garantías de envíos
de mensajes?

Estas preguntas refieren a los problemas principales al implementar
sistemas distribuidos.